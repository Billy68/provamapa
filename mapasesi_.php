<!DOCTYPE html>
<?php

error_reporting(E_ERROR);
ini_set('display_errors', 'on');
date_default_timezone_set('America/Sao_Paulo');

include ('database.php');
$sql = 'SELECT * FROM table2';
$consulta = $conexao->query($sql);
$dado = $consulta->fetchAll(PDO::FETCH_ASSOC);
//print_r($dado);

$dados = array();
require_once('geocode.lib.php');

foreach ($dado as $row) {
	$endereco = $row['end'].'/'.$row['cid'].'/'.$row['uf'];
	$pos = lookup($endereco);
	if ($pos['longitude'] != null){
		$dados[] = array($pos['longitude'],$pos['latitude']);
	}
//	$cont +=1;
//	if ($cont==2){
//		break;
//	}
}

$dataText = json_encode($dados);

?>
<html lang="pt-BR">
<head>
  <meta charset="utf-8">
  <title>Mapas com leaflet</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/favicon.png">
	<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" type="text/css" />
<style type="text/css">
		#map{
			width:100%;
			height:900px;
			padding:0;
			margin:0;
			}
	</style>
</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> 
					 <a class="navbar-brand" href="#">Meu Site</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active">
							<a href="#"></a>
						</li>
						<li>
							<a href="#"></a>
						</li>
							</li>
						<li class="active">
							<a href="#"></a>
						</li>
						<li class="active">
							<a href="#"></a>
						</li>
						<li class="dropdown ">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">One more separated link</a>
								</li>
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> <button type="submit" class="btn btn-default">Submit</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#">Link</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				
			</nav>
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="#">Library</a> <span class="divider">/</span>
				</li>
				<li class="active">
					Data
				</li>
			</ul>
			<div class="page-header">
			</div>
			
			<div id="map"></div>

		<hr/>
		<div class= "foot well">
		<p>&copy; 2015 - JRB </p>

		</div>
	</div>
</div>
 
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js" type="text/javascript" ></script>
	<script type="text/javascript">
		//<![CDATA[
			var dados = <?php echo $dados; ?>;
			var map = L.map('map').setView( [-50,-30]  ,3);
			L.tileLayer('http://tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 12, attribution: 'Map data &copy; 2012 OpenStreetMap contributors' } ).addTo(map);
			//L.tileLayer('http://{s}.www.toolserver.org/tiles/bw-mapnik/{z}/{x}/{y}.png').addTo(map);
			//L.tileLayer('http://a.basemaps.cartocdn.com/dark_all/${z}/${x}/${y}.png').addTo(map);

			<?php foreach ($dados as $i) { ?> 
				 
				L.marker( [<?php echo $i[0]; ?> , <?php echo $i[1]; ?> ] )
					.bindPopup('OK!')
					.openPopup()				
					.addTo(map);
				
				<?php } ?>
		//]]>
	</script>

</body>
</html>
