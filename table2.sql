-- phpMyAdmin SQL Dump
-- version 4.2.11-dev
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 27, 2015 at 10:05 PM
-- Server version: 10.0.18-MariaDB-log
-- PHP Version: 5.6.99-hhvm

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `populacao`
--

-- --------------------------------------------------------

--
-- Table structure for table `table2`
--

CREATE TABLE IF NOT EXISTS `table2` (
`id` int(11) NOT NULL,
  `nome` varchar(51) DEFAULT NULL,
  `end` varchar(63) DEFAULT NULL,
  `cid` varchar(20) DEFAULT NULL,
  `uf` varchar(6) DEFAULT NULL,
  `lat` float(10,6) NOT NULL,
  `lgn` float(10,6) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table2`
--

INSERT INTO `table2` (`id`, `nome`, `end`, `cid`, `uf`, `lat`, `lgn`) VALUES
(2, 'BARRA DA TIJUCA/ESTACIO DE SA CAMPUS TOM JOBIM- RJ ', 'AV. DAS AMERICAS, 4.200 - BL 11 (BARRA SHOPPING) CEP: 22640-907', 'rio de janeiro', 'RJ', 0, 0),
(3, 'CABO FRIO/ESTACIO DE SA - CABO FRIO ', 'ROD. GENERAL ALFREDO BRUNO GOMES MARTINS,sn CEP: 28909-800', 'rio de janeiro', 'RJ', 0, 0),
(4, 'CAMPO GRANDE/MOACYR BASTOS - RJ ', 'RUA ENGENHEIRO TRINDADE , 229 CEP: 23050-290', 'rio de janeiro', 'RJ', 0, 0),
(5, 'Del Castilho / Estacio Nova America ', 'Av. Pastor Martin Luther King Junior 126 CEP: 20765-630', 'rio de janeiro', 'RJ', 0, 0),
(6, 'ITAPERUNA/CENTRO UNIVERSITARIO SAO JOSE ', 'RUA MAJOR PORPHIRIO HENRIQUES, 41 CEP: 28300-000', 'rio de janeiro', 'RJ', 0, 0),
(7, 'MADUREIRA / UNIVERCIDADE - RJ ', 'RUA MINISTRO EDGAR ROMERO, 807 CEP: 21360-201', 'rio de janeiro', 'RJ', 0, 0),
(8, 'POSTO DE ATENDIMENTO BOTAFOGO / IBMR ', 'PRAIA DE BOTAFOGO, 158 - 2o and CEP: 22250-040', 'rio de janeiro', 'RJ', 0, 0),
(9, 'REALENGO - UCB ', 'AV. SANTA CRUZ, 1631 CEP: 21765-000', 'rio de janeiro', 'RJ', 0, 0),
(10, 'RIO COMPRIDO / UNICARIOCA - RJ ', 'AV. PAULO DE FRONTIN, 568 CEP: 20261-543', 'rio de janeiro', 'RJ', 0, 0),
(11, 'RIO DE JANEIRO/RJ ', 'RUA DA CONSTITUICAO, 65 CEP: 20060-010', 'rio de janeiro', 'RJ', 0, 0),
(12, 'TERESOPOLIS ', 'RUA FRANCISCO DE SA, 163  CEP: 25953-011', 'rio de janeiro', 'RJ', 0, 0),
(13, 'BARRA MANSA - RJ ', 'RUA DR. MARIO RAMOS, 145 - LOJA 02 CEP: 27330-231', 'barra mansa', 'RJ', 0, 0),
(14, 'MACAE - RJ ', 'RUA VISCONDE DE QUISSAMA,242 LJ B CEP: 27910-020', 'macae', 'RJ', 0, 0),
(15, 'NITEROI - RJ ', 'RUA MARQUES DE OLINDA, 101 CEP: 24030-170', 'niteroi', 'RJ', 0, 0),
(16, 'RESENDE - RJ ', 'RUA NICOLAU TARANTO, 197 - LJ 02. CEP: 27542-020', 'resende', 'RJ', 0, 0),
(17, 'TRES RIOS - RJ ', 'RUA BARAO DE ENTRE RIOS, 343. CEP: 25802-315', 'tres rios', 'RJ', 0, 0),
(18, 'VOLTA REDONDA - RJ ', 'RUA QUARENTA, 20 SALA 214 CEP: 27180-000', 'volta redonda', 'RJ', 0, 0),
(19, 'DUQUE DE CAXIAS ', 'RUA ELIAS FRANCISCO PARIS,343 3 ANDAR CEP: 25075-110', 'duque de caxias', 'RJ', 0, 0),
(20, 'NOVA FRIBURGO - RJ ', 'RUA LUIZ SPINELLI, 27 SL 203 CEP: 28610-180', 'nova friburgo', 'RJ', 0, 0),
(21, 'NOVA IGUACU - RJ ', 'RUA PROFA VENINA CORREA TORRES, 230 SALA 608 CEP: 26220-100', 'nova igua', 'RJ', 0, 0),
(22, 'PETROPOLIS - RJ ', 'AV. DOM PEDRO I, 374 CEP: 25610-020', 'petropolis', 'RJ', 0, 0),
(23, 'REGIONAL DE CAMPOS - RJ ', 'AV. ALBERTO TORRES, 427 CEP: 28051-286', 'campos', 'RJ', 0, 0),
(24, 'ARACAJU/SE - UNIDADE DE OPERACAO ', 'RUA SILVIO CESAR LEITE, N. 116 CEP: 49020-060', 'aracaju', 'SE', 0, 0),
(25, 'ARAPIRACA ', 'RUA DR. PEDRO CORREIA, 139 CEP: 57300-400', 'arapiraca', 'AL', 0, 0),
(26, 'CAMACARI/BA - UNIDADE DE OPERACAO ', 'AV JORGE AMADO S/N LJ 15 E 16 SHOPPING CAMACARI CEP: 42801-170', 'camacari', 'BA', 0, 0),
(27, 'FEIRA DE SANTANA/ BA - UNIDADE DE OPERACAO ', 'AV. MARIA QUITERIA, 2381 CEP: 44038-030', 'feira de santana', 'BA', 0, 0),
(28, 'ILHEUS ', 'PCA. JOSE MARCELINO, 14 7 ANDAR SL 709,710 CEP: 45653-030', 'ilheus', 'BA', 0, 0),
(29, 'ITABUNA/ BA - UNIDADE DE OPERACAO ', 'AV. DUQUE DE CAXIAS, 359 CEP: 45600-000', 'itabuna', 'BA', 0, 0),
(30, 'MACEIO/ AL - UNIDADE DE OPERACAO ', 'AV MENDONCA JUNIOR, 1190 CEP: 57052-480', 'maceio', 'AL', 0, 0),
(31, 'SALVADOR / BA - UNIDADE DE OPERACAO ', 'AV. TANCREDO NEVES, 450 SL 1502 CEP: 41820-020', 'salvador', 'BA', 0, 0),
(32, 'SERV.MUN. DE ITERM. DE MAO DE OBRA / BA ', 'RUA MIGUEL CALMON, 382 CEP: 40015-010', 'salvador', 'BA', 0, 0),
(33, 'UNIJORGE - UNIDADE SALVADOR ', 'AV.LUIZ VIANA FILHO, 6775 CEP: 41745-130', 'salvador', 'BA', 0, 0),
(34, 'UNIME/BA - UNIAO METROPOLITANA DE EDUCACAO E CULTU ', 'AV. LUIZ TARQUINIO PONTES, 600 CEP: 42700-000', 'salvador', 'BA', 0, 0),
(35, 'VITORIA DA CONQUISTA/BA - UNIDADE DE OPERACAO ', 'AV. VIVALDO MENDES, 908 CEP: 45020-370', 'vitoria da conquista', 'BA', 0, 0),
(36, 'BELO HORIZONTE/MG ', 'RUA CELIO DE CASTRO, 79 CEP: 31110-000', 'belo horizonte', 'MG', 0, 0),
(37, 'CURITIBA/PR ', 'RUA IVO LEAO 42 ALTO DA GLORIA CEP: 80030-180', 'curitiba', 'PR', 0, 0),
(38, 'FLORIANOPOLIS/SC ', 'R.ANTONIO DIB MUSSI 473 CEP: 88015-110', 'florian', 'SC', 0, 0),
(39, 'PORTO ALEGRE/RS ', 'RUA DOM PEDRO II, 861 CEP: 90550-142', 'porto alegre', 'RS', 0, 0),
(40, 'RECIFE/PE ', 'RUA DO PROGRESSO, 465 7a S/705 CEP: 50070-020', 'recife', 'PE', 0, 0),
(41, 'VITORIA/ES ', 'AV. PRINCESA ISABEL, 629 2.ANDAR CEP: 29010-904', 'vitoria', 'ES', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table2`
--
ALTER TABLE `table2`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table2`
--
ALTER TABLE `table2`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
